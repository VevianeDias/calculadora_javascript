<!DOCTYPE html>

<html>
<head>
    <style>
        legend{color:darkblue; font-size: 50px; font-family: Calibri; font-weight: bold}
        label{color:teal; font-size: 20px; font-family: Calibri; font-style: oblique}
        button{color:rgb(4, 5, 83);font-size: 40px; font-family: Calibri; font-weight: bold; margin-top: 2px;margin-right: 5px; margin-left: 20px}
        label1{color:rgb(4, 5, 83);font-size: 40px; font-family: Calibri; font-weight: bold}
        input{font-size: 20px; font-family: Calibri; font-style: oblique}
    </style>
    <meta charset="utf-8" />
    <title>Calculadora_Java</title>    
</head>
    <body>    
        
       
            <legend>Calculadora</legend>
                                
            <div>
                <label for="num1">Digite o primeiro número:</label> 
                <br>
                <input id="num1" type="number" required="" placeholder="Somente Números" name="num1"/> 
                <br>
                <br>
            </div>
            <div> 
                <label for="num2">Digite o segundo número:</label> 
                <br>
                <input id="num2" type="number" required="" placeholder="Somente Números" name="num2"/>
                <br>
            </div>                    
            </br>
            <div>
                <button onclick="somar()">+</button>
                <button onclick="subtrair()">-</button>
                <button onclick="multiplicar()">x</button>
                <button onclick="dividir()">/</button>
            </div>                   
            </br>
            <div> <label1 for="resultado">=</label1> <input id="resultado" type="number" name="resultado"></div>                    
       
     
        <script>
            function somar() { 
                var num1 = +document.getElementById("num1").value;
                var num2 = +document.getElementById("num2").value;

                var resultado = document.getElementById("resultado");

                resultado.value = num1 + num2;
                
            };

            function subtrair() { 
                var num1 = +document.getElementById("num1").value;
                var num2 = +document.getElementById("num2").value;

                var resultado = document.getElementById("resultado");

                resultado.value = num1 - num2;
            };

            function multiplicar() { 
                var num1 = +document.getElementById("num1").value;
                var num2 = +document.getElementById("num2").value;

                var resultado = document.getElementById("resultado");

                resultado.value = num1 * num2;
            };  

            function dividir() { 
                var num1 = +document.getElementById("num1").value;
                var num2 = +document.getElementById("num2").value;

                var resultado = document.getElementById("resultado");

                resultado.value = num1 / num2;
            };

        </script>

    </body>
</html>    
